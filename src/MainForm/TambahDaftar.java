package MainForm;

import java.sql.*;
import MainForm.config.Koneksi;
import com.raven.datechooser.SelectedDate;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;

public class TambahDaftar extends javax.swing.JFrame {

    Connection conn = Koneksi.getKoneksi();
    ResultSet rs = null;
    PreparedStatement pst = null;
    
    private static final int max_nik = 16;

    
    public TambahDaftar() {
        initComponents();
        updateTable();
        setLocationRelativeTo(null);
        setIcon();
        reset();
    }
    
    private void setIcon(){
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/Icon/144noBg.png")));
    }
    
    private void reset(){ //untuk mereset data saat sudah diinput
        tfNama.setText(null);
        tfNIK.setText(null);
        tfNoHp.setText(null);
    }
    
    private void updateTable(){
        String sql = "select * from sistemkost.data_kamar";
        try {
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            cmbTipe.removeAllItems();
            while (rs.next()){
                String jenisKamar = rs.getString("jenis");
                cmbTipe.addItem(jenisKamar);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        date = new com.raven.datechooser.DateChooser();
        jPanelGradient1 = new Custom.JPanelGradient();
        panelCustom1 = new Custom.PanelCustom();
        txtNama = new javax.swing.JLabel();
        tfNama = new fosalgo.FTextField();
        txtNIK = new javax.swing.JLabel();
        tfNIK = new fosalgo.FTextField();
        tfNoHp = new fosalgo.FTextField();
        txtNoHp = new javax.swing.JLabel();
        txtStatus = new javax.swing.JLabel();
        txtPekerjaan = new javax.swing.JLabel();
        btnTambah = new Custom.FButton();
        btnBatal = new Custom.FButton();
        cmbStatus = new javax.swing.JComboBox<>();
        txtStatus1 = new javax.swing.JLabel();
        cmbKesibukan = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        cmbTipe = new javax.swing.JComboBox<>();
        rbGenderLk = new javax.swing.JRadioButton();
        rbGenderPr = new javax.swing.JRadioButton();
        jLabel3 = new javax.swing.JLabel();
        tfTarif = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        tfSewa = new fosalgo.FTextField();
        jSeparator1 = new javax.swing.JSeparator();
        tfTanggal = new fosalgo.FTextField();
        btnTampilTgl = new javax.swing.JButton();
        btnOk = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();

        date.setForeground(new java.awt.Color(83, 105, 118));
        date.setTextRefernce(tfTanggal);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelGradient1.setColorEnd(new java.awt.Color(41, 46, 73));
        jPanelGradient1.setColorStart(new java.awt.Color(83, 105, 118));

        panelCustom1.setBackground(new java.awt.Color(255, 255, 255));
        panelCustom1.setRoundBotLeft(50);
        panelCustom1.setRoundBotRight(50);
        panelCustom1.setRoundTopLeft(50);
        panelCustom1.setRoundTopRight(50);

        txtNama.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        txtNama.setForeground(new java.awt.Color(0, 0, 0));
        txtNama.setText("Nama");

        tfNama.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        tfNama.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tfNamaKeyPressed(evt);
            }
        });

        txtNIK.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        txtNIK.setForeground(new java.awt.Color(0, 0, 0));
        txtNIK.setText("NIK");

        tfNIK.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        tfNIK.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tfNIKKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tfNIKKeyTyped(evt);
            }
        });

        tfNoHp.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        tfNoHp.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tfNoHpKeyPressed(evt);
            }
        });

        txtNoHp.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        txtNoHp.setForeground(new java.awt.Color(0, 0, 0));
        txtNoHp.setText("No Hp");

        txtStatus.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        txtStatus.setForeground(new java.awt.Color(0, 0, 0));
        txtStatus.setText("Status");

        txtPekerjaan.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        txtPekerjaan.setForeground(new java.awt.Color(0, 0, 0));
        txtPekerjaan.setText("Kesibukan");

        btnTambah.setText("Tambah");
        btnTambah.setFillOver(new java.awt.Color(255, 0, 51));
        btnTambah.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        btnTambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTambahActionPerformed(evt);
            }
        });

        btnBatal.setText("Batal");
        btnBatal.setFillOver(new java.awt.Color(255, 0, 0));
        btnBatal.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        btnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBatalActionPerformed(evt);
            }
        });

        cmbStatus.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        cmbStatus.setForeground(new java.awt.Color(0, 0, 0));
        cmbStatus.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Menikah", "Belum Menikah" }));
        cmbStatus.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbStatusKeyPressed(evt);
            }
        });

        txtStatus1.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        txtStatus1.setForeground(new java.awt.Color(0, 0, 0));
        txtStatus1.setText("Gender");

        cmbKesibukan.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        cmbKesibukan.setForeground(new java.awt.Color(0, 0, 0));
        cmbKesibukan.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Bekerja", "Mahasiswa", "Lainnya" }));
        cmbKesibukan.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbKesibukanKeyPressed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("Tipe Kamar");

        cmbTipe.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        cmbTipe.setForeground(new java.awt.Color(0, 0, 0));
        cmbTipe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTipeActionPerformed(evt);
            }
        });
        cmbTipe.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                cmbTipeKeyPressed(evt);
            }
        });

        rbGenderLk.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(rbGenderLk);
        rbGenderLk.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        rbGenderLk.setForeground(new java.awt.Color(0, 0, 0));
        rbGenderLk.setText("Laki-Laki");
        rbGenderLk.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                rbGenderLkKeyPressed(evt);
            }
        });

        rbGenderPr.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(rbGenderPr);
        rbGenderPr.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        rbGenderPr.setForeground(new java.awt.Color(0, 0, 0));
        rbGenderPr.setText("Perempuan");

        jLabel3.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("Tarif");

        tfTarif.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        tfTarif.setForeground(new java.awt.Color(0, 0, 0));

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Lucida Fax", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("Masukkan Data Indekost");

        jLabel5.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 0, 0));
        jLabel5.setText("Check-In");

        jLabel4.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 0, 0));
        jLabel4.setText("Durasi Sewa");

        tfSewa.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        tfSewa.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tfSewaKeyPressed(evt);
            }
        });

        tfTanggal.setForeground(new java.awt.Color(0, 0, 0));
        tfTanggal.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        tfTanggal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tfTanggalKeyPressed(evt);
            }
        });

        btnTampilTgl.setText("|||");
        btnTampilTgl.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTampilTglActionPerformed(evt);
            }
        });

        btnOk.setFont(new java.awt.Font("Lucida Fax", 1, 12)); // NOI18N
        btnOk.setText("Ok");

        jLabel7.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(0, 0, 0));
        jLabel7.setText("Bulan");

        jLabel6.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 0, 0));
        jLabel6.setText("per Bulan");

        javax.swing.GroupLayout panelCustom1Layout = new javax.swing.GroupLayout(panelCustom1);
        panelCustom1.setLayout(panelCustom1Layout);
        panelCustom1Layout.setHorizontalGroup(
            panelCustom1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCustom1Layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(btnTambah, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnBatal, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39))
            .addGroup(panelCustom1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSeparator1)
                .addContainerGap())
            .addGroup(panelCustom1Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panelCustom1Layout.createSequentialGroup()
                .addGap(57, 57, 57)
                .addGroup(panelCustom1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCustom1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(tfNoHp, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCustom1Layout.createSequentialGroup()
                        .addComponent(txtNIK)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(tfNIK, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelCustom1Layout.createSequentialGroup()
                        .addComponent(txtNama)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 89, Short.MAX_VALUE)
                        .addComponent(tfNama, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCustom1Layout.createSequentialGroup()
                        .addGroup(panelCustom1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel2)
                            .addComponent(jLabel5)
                            .addComponent(txtNoHp))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(panelCustom1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(tfTanggal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(tfSewa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbTipe, 0, 122, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panelCustom1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelCustom1Layout.createSequentialGroup()
                                .addComponent(btnTampilTgl, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(btnOk, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel7))))
                .addGap(18, 18, 18)
                .addGroup(panelCustom1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtStatus1)
                    .addComponent(txtPekerjaan)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtStatus))
                .addGap(18, 18, 18)
                .addGroup(panelCustom1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmbKesibukan, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelCustom1Layout.createSequentialGroup()
                        .addGroup(panelCustom1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rbGenderLk)
                            .addComponent(tfTarif, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panelCustom1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rbGenderPr)
                            .addComponent(jLabel6))))
                .addContainerGap())
        );

        panelCustom1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnBatal, btnTambah});

        panelCustom1Layout.setVerticalGroup(
            panelCustom1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCustom1Layout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addGroup(panelCustom1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNama)
                    .addComponent(tfNama, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtStatus)
                    .addComponent(cmbStatus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelCustom1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNIK)
                    .addComponent(tfNIK, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPekerjaan)
                    .addComponent(cmbKesibukan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelCustom1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNoHp)
                    .addComponent(tfNoHp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtStatus1)
                    .addComponent(rbGenderLk, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(rbGenderPr))
                .addGap(18, 18, 18)
                .addGroup(panelCustom1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cmbTipe, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(tfTarif, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(18, 18, 18)
                .addGroup(panelCustom1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(tfSewa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addGap(18, 18, 18)
                .addGroup(panelCustom1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(tfTanggal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnTampilTgl, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnOk, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 80, Short.MAX_VALUE)
                .addGroup(panelCustom1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTambah, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBatal, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(21, Short.MAX_VALUE))
        );

        panelCustom1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {rbGenderLk, rbGenderPr});

        panelCustom1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnOk, btnTampilTgl});

        panelCustom1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cmbKesibukan, cmbStatus, cmbTipe, tfNIK, tfNama, tfNoHp, tfSewa, tfTanggal, tfTarif});

        javax.swing.GroupLayout jPanelGradient1Layout = new javax.swing.GroupLayout(jPanelGradient1);
        jPanelGradient1.setLayout(jPanelGradient1Layout);
        jPanelGradient1Layout.setHorizontalGroup(
            jPanelGradient1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelGradient1Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(panelCustom1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(25, Short.MAX_VALUE))
        );
        jPanelGradient1Layout.setVerticalGroup(
            jPanelGradient1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelGradient1Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(panelCustom1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(25, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelGradient1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelGradient1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnTambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTambahActionPerformed
        String sql = "INSERT INTO sistemkost.data_indekos(nama, nik, hp, status, kesibukan, gender, tipe, harga, tanggal, durasi, total) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try{
            
            SelectedDate selectedDate = date.getSelectedDate();
            String dateString = selectedDate.getDay() + "-" + selectedDate.getMonth() + "-" + selectedDate.getYear();
            
            int durasi = Integer.parseInt(tfSewa.getText());
            
            long nik = Long.parseLong(tfNIK.getText());
            long noHp = Long.parseLong(tfNoHp.getText());
            long harga = Long.parseLong(tfTarif.getText());
            String gender = rbGenderLk.isSelected() ? "Laki-Laki" : "Perempuan";

            long totalByr = harga * durasi;
            
            pst = conn.prepareStatement(sql);
            pst.setString(1, tfNama.getText());
            pst.setLong(2, nik);
            pst.setLong(3, noHp);
            pst.setString(4, (String) cmbStatus.getSelectedItem());
            pst.setString(5, (String) cmbKesibukan.getSelectedItem());
            pst.setString(6, (String) gender);
            pst.setString(7, (String) cmbTipe.getSelectedItem());
            pst.setLong(8, harga);
            pst.setString(9, dateString);
            pst.setInt(10, durasi);
            pst.setLong(11, totalByr);
            
            int jumlahBaris = pst.executeUpdate();
            
            if(jumlahBaris > 0){
                JOptionPane.showMessageDialog(null, "Data Tersimpan");
                Penyewa indekos = new Penyewa();
                indekos.setVisible(true);
                this.dispose();
            } else {
                JOptionPane.showMessageDialog(null, "Data Gagal Disimpan");
            }
            
            }catch (SQLException | HeadlessException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Terjadi Kesalahan Umum:" + e.getMessage());
        }
        reset();
    }//GEN-LAST:event_btnTambahActionPerformed

    private void btnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBatalActionPerformed
        reset();
        Penyewa indekos = new Penyewa();
        indekos.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnBatalActionPerformed

    private void cmbTipeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTipeActionPerformed
        String sql = "SELECT harga FROM data_kamar WHERE jenis=?";
        try {
            String selectedTipe = (String) cmbTipe.getSelectedItem();
            
            if (selectedTipe != null) {
                pst = conn.prepareStatement(sql);
                pst.setString(1, selectedTipe);
                ResultSet rs = pst.executeQuery();
                if(rs.next()) {
                    tfTarif.setText(rs.getString("harga"));
                } else {
                    tfTarif.setText("");
                }
            } else {
                tfTarif.setText("");
            }
        } catch (Exception e){
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
    }//GEN-LAST:event_cmbTipeActionPerformed

    private void btnTampilTglActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTampilTglActionPerformed
        date.showPopup();
    }//GEN-LAST:event_btnTampilTglActionPerformed

    private void tfNamaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfNamaKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_ENTER)
            tfNIK.requestFocus();
    }//GEN-LAST:event_tfNamaKeyPressed

    private void tfNIKKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfNIKKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_ENTER)
            tfNoHp.requestFocus();
    }//GEN-LAST:event_tfNIKKeyPressed

    private void tfNoHpKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfNoHpKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_ENTER)
            cmbTipe.requestFocus();
    }//GEN-LAST:event_tfNoHpKeyPressed

    private void cmbTipeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbTipeKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_ENTER)
            tfSewa.requestFocus();
    }//GEN-LAST:event_cmbTipeKeyPressed

    private void tfSewaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfSewaKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_ENTER)
            tfTanggal.requestFocus();
    }//GEN-LAST:event_tfSewaKeyPressed

    private void tfTanggalKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfTanggalKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_ENTER)
            cmbStatus.requestFocus();
    }//GEN-LAST:event_tfTanggalKeyPressed

    private void cmbStatusKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbStatusKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_ENTER)
            cmbKesibukan.requestFocus();
    }//GEN-LAST:event_cmbStatusKeyPressed

    private void cmbKesibukanKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cmbKesibukanKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_ENTER)
            btnTambahActionPerformed(null);
    }//GEN-LAST:event_cmbKesibukanKeyPressed

    private void rbGenderLkKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rbGenderLkKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_ENTER)
            btnTambahActionPerformed(null);
    }//GEN-LAST:event_rbGenderLkKeyPressed

    private void tfNIKKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfNIKKeyTyped
        if (tfNIK.getText().length() >= max_nik) {
            evt.consume();
        }
    }//GEN-LAST:event_tfNIKKeyTyped

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TambahDaftar().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private Custom.FButton btnBatal;
    private javax.swing.JButton btnOk;
    private Custom.FButton btnTambah;
    private javax.swing.JButton btnTampilTgl;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> cmbKesibukan;
    private javax.swing.JComboBox<String> cmbStatus;
    private javax.swing.JComboBox<String> cmbTipe;
    private com.raven.datechooser.DateChooser date;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private Custom.JPanelGradient jPanelGradient1;
    private javax.swing.JSeparator jSeparator1;
    private Custom.PanelCustom panelCustom1;
    private javax.swing.JRadioButton rbGenderLk;
    private javax.swing.JRadioButton rbGenderPr;
    private fosalgo.FTextField tfNIK;
    private fosalgo.FTextField tfNama;
    private fosalgo.FTextField tfNoHp;
    private fosalgo.FTextField tfSewa;
    private fosalgo.FTextField tfTanggal;
    private javax.swing.JTextField tfTarif;
    private javax.swing.JLabel txtNIK;
    private javax.swing.JLabel txtNama;
    private javax.swing.JLabel txtNoHp;
    private javax.swing.JLabel txtPekerjaan;
    private javax.swing.JLabel txtStatus;
    private javax.swing.JLabel txtStatus1;
    // End of variables declaration//GEN-END:variables
}
