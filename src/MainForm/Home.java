package MainForm;

import MainForm.SideBar.Header;
import java.awt.Color;
import java.awt.Toolkit;
import javaswingdev.drawer.Drawer;
import javaswingdev.drawer.DrawerController;
import javaswingdev.drawer.DrawerItem;
import javaswingdev.drawer.EventDrawer;
import javax.swing.ImageIcon;

public class Home extends javax.swing.JFrame {
    
    private DrawerController drawer;  
    
    public Home() {
        initComponents();
        setIcon();
        SideBar();
    }
    
    private void setIcon(){
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/Icon/144noBg.png")));
    }
    
    private void SideBar(){
        drawer = Drawer.newDrawer(this)
                .header(new Header())
                .space(5)
                .background(new Color(204, 142, 53))
                .backgroundTransparent(0.3f)
                .drawerWidth(230)
                .enableScroll(true)
                .addChild(new DrawerItem("Home").icon(new ImageIcon(getClass().getResource("/Icon/Home.png"))).build())
                .addChild(new DrawerItem("Indekos Baru").icon(new ImageIcon(getClass().getResource("/Icon/Add Administrator.png"))).build())
                .addChild(new DrawerItem("Manage Indekos").icon(new ImageIcon(getClass().getResource("/Icon/People.png"))).build())
                .addChild(new DrawerItem("Manage Kamar").icon(new ImageIcon(getClass().getResource("/Icon/Room.png"))).build())
                .addChild(new DrawerItem("Manage User").icon(new ImageIcon(getClass().getResource("/Icon/User.png"))).build())
                .addFooter(new DrawerItem("Exit").icon(new ImageIcon(getClass().getResource("/Icon/Logout.png"))).build())
                .event(new EventDrawer() {
            @Override
            public void selected(int i, DrawerItem di) {
                PilihItem(di);
            }
                })
                .build();
    }
    
    private void PilihItem(DrawerItem drawerItem){
        String itemName = drawerItem.getText();
        switch (itemName) {
            case "Home":
                new Home().setVisible(true);
                this.dispose();
                break;
            case "Indekos Baru" :
                new TambahDaftar().setVisible(true);
                this.dispose();
                break;
            case "Manage Indekos":
                new Penyewa().setVisible(true);
                this.dispose();
                break;
            case "Manage Kamar":
                new DataKamar().setVisible(true);
                this.dispose();
                break;
            case "Manage User":
                new Admin().setVisible(true);
                this.dispose();
                break;
            case "Exit":
                new Login().setVisible(true);
                this.dispose();
                break;
            default:
                System.out.println("Invalid selection");
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        BG = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jButton1.setBackground(new java.awt.Color(255, 255, 255));
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icon/Menu_1.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, -1, -1));

        BG.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Icon/Background (1).jpg"))); // NOI18N
        getContentPane().add(BG, new org.netbeans.lib.awtextra.AbsoluteConstraints(-110, -70, 1080, 680));

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if(drawer.isShow()){
            drawer.hide();
        } else {
            drawer.show();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Home.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Home().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel BG;
    private javax.swing.JButton jButton1;
    // End of variables declaration//GEN-END:variables

}
