package MainForm;

import java.sql.*;
import MainForm.config.Koneksi;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import table.TableCustom;

public class DataKamar extends javax.swing.JFrame {

    Connection conn = Koneksi.getKoneksi();
    ResultSet rs = null;
    PreparedStatement pst = null;
    
    public DataKamar() {
        initComponents();
        setLocationRelativeTo(null);
        menampilkanTable();
        setIcon();
        
        CTable.apply(jScrollPane1, TableCustom.TableType.DEFAULT);
    }
    
    private void setIcon(){
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/Icon/144noBg.png")));
    }
    
    private void reset(){ //untuk mereset data saat sudah diinput
        tfTipeKamar.setText(null);
        tfHarga.setText(null);
        tfJumlahKamar.setText(null);
    }
    
    public void menampilkanTable() {
        DefaultTableModel tb = new DefaultTableModel();
        tb.addColumn("Tipe Kamar");
        tb.addColumn("Harga");
        tb.addColumn("Kamar Tersedia");
        jTable1.setModel(tb);
        try{
            String sql = "select * from data_kamar";
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()){
                tb.addRow(new Object[]{
                rs.getString("jenis"),
                rs.getString("harga"),
                rs.getString("jumlah"),
                });            
        } 
        }catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Data Gagal Ditampilkan" + e.getMessage());
    }
    }
    
    private void deleteFromDatabase(Object primaryKey) {
    try {
        String deleteQuery = "DELETE FROM data_kamar WHERE jenis = ?";
        pst = conn.prepareStatement(deleteQuery);
        pst.setObject(1, primaryKey);
        int kolomDipilih = pst.executeUpdate();
        
        System.out.println("Kolom Dipilih" + kolomDipilih);
        System.out.println("Prymary Key Dihapus" + primaryKey);
        
        if (kolomDipilih > 0){
            JOptionPane.showMessageDialog(this, "Data berhasil dihapus dari database.", "Sukses", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(this, "Data tidak ditemukan dalam database.", "Peringatan", JOptionPane.WARNING_MESSAGE);
        }
    } catch (SQLException e) {
        e.printStackTrace();
        JOptionPane.showMessageDialog(this, "Error menghapus data dari database.", "Error", JOptionPane.ERROR_MESSAGE);
    }
}
    
    private void refreshData() {
    DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
    model.setRowCount(0); // Hapus semua baris saat ini dari tabel
    // Panggil kembali metode untuk menampilkan data baru ke dalam tabel
    menampilkanTable();
}

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        CTable = new table.TableCustom();
        jPanel1 = new javax.swing.JPanel();
        jPanelGradient1 = new Custom.JPanelGradient();
        jLabel2 = new javax.swing.JLabel();
        tfTipeKamar = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        btnSimpan = new Custom.FButton();
        btnBersih = new Custom.FButton();
        jLabel6 = new javax.swing.JLabel();
        tfHarga = new javax.swing.JTextField();
        tfJumlahKamar = new javax.swing.JTextField();
        jPanelGradient2 = new Custom.JPanelGradient();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        btnHome = new Custom.FButton();
        btnHapus = new Custom.FButton();
        btnRefresh = new Custom.FButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanelGradient1.setColorEnd(new java.awt.Color(74, 105, 189));
        jPanelGradient1.setColorStart(new java.awt.Color(204, 204, 204));

        jLabel2.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("Tipe Kamar");

        tfTipeKamar.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        tfTipeKamar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tfTipeKamarKeyPressed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("Harga");

        btnSimpan.setText("Simpan");
        btnSimpan.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        btnSimpan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSimpanActionPerformed(evt);
            }
        });

        btnBersih.setText("Bersih");
        btnBersih.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N

        jLabel6.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 0, 0));
        jLabel6.setText("Jumlah Kamar");

        tfHarga.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        tfHarga.setForeground(new java.awt.Color(0, 0, 0));
        tfHarga.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tfHargaKeyPressed(evt);
            }
        });

        tfJumlahKamar.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        tfJumlahKamar.setForeground(new java.awt.Color(0, 0, 0));
        tfJumlahKamar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tfJumlahKamarKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanelGradient1Layout = new javax.swing.GroupLayout(jPanelGradient1);
        jPanelGradient1.setLayout(jPanelGradient1Layout);
        jPanelGradient1Layout.setHorizontalGroup(
            jPanelGradient1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelGradient1Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(jPanelGradient1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelGradient1Layout.createSequentialGroup()
                        .addComponent(btnSimpan, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnBersih, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(tfTipeKamar, javax.swing.GroupLayout.DEFAULT_SIZE, 266, Short.MAX_VALUE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(tfHarga)
                    .addComponent(jLabel6)
                    .addComponent(tfJumlahKamar))
                .addContainerGap(20, Short.MAX_VALUE))
        );
        jPanelGradient1Layout.setVerticalGroup(
            jPanelGradient1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelGradient1Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tfTipeKamar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tfHarga, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tfJumlahKamar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 132, Short.MAX_VALUE)
                .addGroup(jPanelGradient1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnBersih, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSimpan, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28))
        );

        jPanelGradient1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {tfHarga, tfJumlahKamar});

        jPanelGradient2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0), 3));
        jPanelGradient2.setColorEnd(new java.awt.Color(41, 46, 73));
        jPanelGradient2.setColorStart(new java.awt.Color(20, 136, 204));

        jLabel1.setFont(new java.awt.Font("Lucida Fax", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("Manajemen Data Kamar");

        javax.swing.GroupLayout jPanelGradient2Layout = new javax.swing.GroupLayout(jPanelGradient2);
        jPanelGradient2.setLayout(jPanelGradient2Layout);
        jPanelGradient2Layout.setHorizontalGroup(
            jPanelGradient2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelGradient2Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanelGradient2Layout.setVerticalGroup(
            jPanelGradient2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelGradient2Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jLabel1)
                .addContainerGap(17, Short.MAX_VALUE))
        );

        jTable1.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Tipe Kamar", "Harga", "Kamar Tersedia"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        btnHome.setText("Home");
        btnHome.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        btnHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHomeActionPerformed(evt);
            }
        });

        btnHapus.setText("Hapus");
        btnHapus.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        btnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHapusActionPerformed(evt);
            }
        });

        btnRefresh.setText("Refresh");
        btnRefresh.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        btnRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRefreshActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelGradient2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanelGradient1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 547, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btnRefresh, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnHapus, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnHome, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jPanelGradient2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanelGradient1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnHome, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
                            .addComponent(btnHapus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnRefresh, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSimpanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSimpanActionPerformed
        String sql = "INSERT INTO sistemkost.data_kamar(jenis, harga, jumlah) VALUES(?, ?, ?)";
        try{
            String tipe_kamar = tfTipeKamar.getText();
            long harga_kamar = Long.parseLong(tfHarga.getText());
            int jumlah_kamar = Integer.parseInt(tfJumlahKamar.getText());
            
            pst = conn.prepareStatement(sql);
            pst.setString(1, tipe_kamar);
            pst.setLong(2, harga_kamar);
            pst.setInt(3, jumlah_kamar);
            
            int jumlahBaris = pst.executeUpdate();
            
            if(jumlahBaris > 0){
                JOptionPane.showMessageDialog(null, "Data Tersimpan");
            } else {
                JOptionPane.showMessageDialog(null, "Data Gagal Disimpan");
            }
        }catch (SQLException | HeadlessException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Terjadi Kesalahan Umum:" + e.getMessage());
        }
        reset();
    }//GEN-LAST:event_btnSimpanActionPerformed

    private void btnHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHomeActionPerformed
        new Home().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnHomeActionPerformed

    private void btnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHapusActionPerformed
        DefaultTableModel model = (DefaultTableModel) jTable1.getModel();
        if(jTable1.getSelectedRow()==-1){
            if(jTable1.getRowCount()==0){
                JOptionPane.showMessageDialog(null, "Tidak Ada Data");
            }else {
                JOptionPane.showMessageDialog(null, "Pilih Data Yang Akan Dihapus");
            }
        } else {
            Object primaryKey = jTable1.getValueAt(jTable1.getSelectedRow(), 0);
            model.removeRow(jTable1.getSelectedRow());
            deleteFromDatabase(primaryKey);
        } 
    }//GEN-LAST:event_btnHapusActionPerformed

    private void btnRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRefreshActionPerformed
        refreshData();
    }//GEN-LAST:event_btnRefreshActionPerformed

    private void tfTipeKamarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfTipeKamarKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_ENTER)
            tfHarga.requestFocus();
    }//GEN-LAST:event_tfTipeKamarKeyPressed

    private void tfHargaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfHargaKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_ENTER)
            tfJumlahKamar.requestFocus();
    }//GEN-LAST:event_tfHargaKeyPressed

    private void tfJumlahKamarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfJumlahKamarKeyPressed
        if(evt.getKeyCode() == KeyEvent.VK_ENTER)
        btnSimpanActionPerformed(null);
    }//GEN-LAST:event_tfJumlahKamarKeyPressed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new DataKamar().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private table.TableCustom CTable;
    private Custom.FButton btnBersih;
    private Custom.FButton btnHapus;
    private Custom.FButton btnHome;
    private Custom.FButton btnRefresh;
    private Custom.FButton btnSimpan;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private Custom.JPanelGradient jPanelGradient1;
    private Custom.JPanelGradient jPanelGradient2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField tfHarga;
    private javax.swing.JTextField tfJumlahKamar;
    private javax.swing.JTextField tfTipeKamar;
    // End of variables declaration//GEN-END:variables
}
