package MainForm;

import MainForm.config.Koneksi;
import java.awt.Toolkit;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.sql.*;
import javax.swing.RowFilter;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;
import table.TableCustom;

public class Penyewa extends javax.swing.JFrame {

    Connection conn = Koneksi.getKoneksi();
    ResultSet rs = null;
    PreparedStatement pst = null;

    private final int columnWidht = 130;
    
    public Penyewa() {
        initComponents();
        menampilkanTable();
        setLocationRelativeTo(null);
        setIcon();
        
        CTable.apply(jScrollPane1, TableCustom.TableType.DEFAULT);
    }
    
    private void setIcon(){
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/Icon/144noBg.png")));
    }
    
    public void menampilkanTable() {
        DefaultTableModel tb = new DefaultTableModel();
        tb.addColumn("NIK");
        tb.addColumn("Nama");
        tb.addColumn("No Handphone");
        tb.addColumn("Kesibukan");
        tb.addColumn("Gender");
        tb.addColumn("Status");
        tb.addColumn("Tipe Kamar");
        tb.addColumn("Check In");
        tb.addColumn("Lama Sewa");
        tb.addColumn("Harga");
        tb.addColumn("Total Biaya");
        daftarTable.setModel(tb);
        try{
            String sql = "select * from data_indekos";
            pst = conn.prepareStatement(sql);
            rs = pst.executeQuery();
            while (rs.next()){
                tb.addRow(new Object[]{
                rs.getString("nik"),
                rs.getString("nama"),
                rs.getString("hp"),
                rs.getString("kesibukan"),
                rs.getString("gender"),
                rs.getString("status"),
                rs.getString("tipe"),
                rs.getString("tanggal"),
                rs.getString("durasi"),
                rs.getString("harga"),
                rs.getString("total")
                });            
        } 
        }catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Data Gagal Ditampilkan" + e.getMessage());
    }
    }
    
    private void editSelectedRow() {
    int selectedRow = daftarTable.getSelectedRow();
    if (selectedRow != -1) {
        int selectedColumn = daftarTable.getSelectedColumn();
        if (selectedColumn != -1) {
            String columnName = daftarTable.getColumnName(selectedColumn);
            String newValue = JOptionPane.showInputDialog(this, "Masukkan nilai baru untuk kolom " + columnName + ":", daftarTable.getValueAt(selectedRow, selectedColumn));
            
            // Perbarui nilai di tabel
            daftarTable.setValueAt(newValue, selectedRow, selectedColumn);

            // Perbarui data di database (ganti dengan metode sesuai kebutuhan)
            updateDataInDatabase(
                    daftarTable.getValueAt(selectedRow, 0).toString(), // NIK sebagai primary key
                    columnName,
                    newValue
            );
        } else {
            JOptionPane.showMessageDialog(null, "Pilih Kolom Yang Akan Diubah");
        }
    } else {
        JOptionPane.showMessageDialog(null, "Pilih Data Yang Akan Diubah");
    }
}
    
    private void updateDataInDatabase(String nik, String columnName, String newValue) {
    try {
        String updateQuery = "UPDATE data_indekos SET " + columnName + "=? WHERE nik=?";
        pst = conn.prepareStatement(updateQuery);
        pst.setString(1, newValue);
        pst.setString(2, nik);

        int rowsAffected = pst.executeUpdate();

        if (rowsAffected > 0) {
            JOptionPane.showMessageDialog(this, "Data berhasil diubah.", "Sukses", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(this, "Tidak ada baris yang terpengaruh. Data mungkin tidak ditemukan.", "Peringatan", JOptionPane.WARNING_MESSAGE);
        }
    } catch (SQLException e) {
        e.printStackTrace();
        JOptionPane.showMessageDialog(this, "Error mengubah data: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
    }
}
    
    private void performSearch(){
        DefaultTableModel df = (DefaultTableModel) daftarTable.getModel();
        TableRowSorter<DefaultTableModel> obj = new TableRowSorter<>(df);
        daftarTable.setRowSorter(obj);
        obj.setRowFilter(RowFilter.regexFilter("(?i)" + tfCari.getText()));
        // "(?i)" untuk konversi keyword ke huruf kecil
        // "(^)" untuk memfilter agar hanya kata depan saja yang muncul
    }
    
    private void hapusDataDB(Object primaryKey) {
    try {
        String deleteQuery = "DELETE FROM data_indekos WHERE nik = ?";
        pst = conn.prepareStatement(deleteQuery);
        pst.setObject(1, primaryKey);
        int kolomDipilih = pst.executeUpdate();
        
        System.out.println("Kolom Dipilih" + kolomDipilih);
        System.out.println("Prymary Key Dihapus" + primaryKey);
        
        if (kolomDipilih > 0){
            JOptionPane.showMessageDialog(this, "Data berhasil dihapus dari database.", "Sukses", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(this, "Data tidak ditemukan dalam database.", "Peringatan", JOptionPane.WARNING_MESSAGE);
        }
    } catch (SQLException e) {
        e.printStackTrace();
        JOptionPane.showMessageDialog(this, "Error menghapus data dari database.", "Error", JOptionPane.ERROR_MESSAGE);
    }
}

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        CTable = new table.TableCustom();
        jPanelGradient1 = new Custom.JPanelGradient();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        btnTambah = new Custom.FButton();
        tfCari = new javax.swing.JTextField();
        btnCari = new Custom.FButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        daftarTable = new javax.swing.JTable();
        fButton3 = new Custom.FButton();
        btnHapus = new Custom.FButton();
        btnEdit = new Custom.FButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanelGradient1.setColorEnd(new java.awt.Color(41, 46, 73));
        jPanelGradient1.setColorStart(new java.awt.Color(20, 136, 204));

        javax.swing.GroupLayout jPanelGradient1Layout = new javax.swing.GroupLayout(jPanelGradient1);
        jPanelGradient1.setLayout(jPanelGradient1Layout);
        jPanelGradient1Layout.setHorizontalGroup(
            jPanelGradient1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanelGradient1Layout.setVerticalGroup(
            jPanelGradient1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 88, Short.MAX_VALUE)
        );

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Lucida Fax", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("Data Penyewa");

        btnTambah.setText("Tambah");
        btnTambah.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        btnTambah.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTambahActionPerformed(evt);
            }
        });

        tfCari.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tfCariMouseClicked(evt);
            }
        });
        tfCari.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                tfCariKeyTyped(evt);
            }
        });

        btnCari.setText("Cari");
        btnCari.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        btnCari.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCariActionPerformed(evt);
            }
        });

        daftarTable.setAutoCreateRowSorter(true);
        daftarTable.setBackground(new java.awt.Color(255, 255, 255));
        daftarTable.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        daftarTable.setFont(new java.awt.Font("Lucida Fax", 0, 12)); // NOI18N
        daftarTable.setForeground(new java.awt.Color(0, 0, 0));
        daftarTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        daftarTable.addContainerListener(new java.awt.event.ContainerAdapter() {
            public void componentAdded(java.awt.event.ContainerEvent evt) {
                daftarTableComponentAdded(evt);
            }
        });
        jScrollPane1.setViewportView(daftarTable);

        fButton3.setText("Home");
        fButton3.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        fButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fButton3ActionPerformed(evt);
            }
        });

        btnHapus.setText("Hapus");
        btnHapus.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        btnHapus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHapusActionPerformed(evt);
            }
        });

        btnEdit.setText("Edit");
        btnEdit.setFont(new java.awt.Font("Lucida Fax", 1, 14)); // NOI18N
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(tfCari, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnCari, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30)))
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(fButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30))))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1222, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnTambah, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnHapus, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(30, Short.MAX_VALUE))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnEdit, btnHapus});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(fButton3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnTambah, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(tfCari, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCari, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnHapus, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 356, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnTambah, fButton3});

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnEdit, btnHapus});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanelGradient1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanelGradient1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnTambahActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTambahActionPerformed
        TambahDaftar tambah = new TambahDaftar();
        tambah.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnTambahActionPerformed

    private void fButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fButton3ActionPerformed
        new Home().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_fButton3ActionPerformed

    private void daftarTableComponentAdded(java.awt.event.ContainerEvent evt) {//GEN-FIRST:event_daftarTableComponentAdded
        // TODO add your handling code here:
    }//GEN-LAST:event_daftarTableComponentAdded

    private void btnHapusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHapusActionPerformed
        DefaultTableModel model = (DefaultTableModel) daftarTable.getModel();
        if(daftarTable.getSelectedRow()==-1){
            if(daftarTable.getRowCount()==0){
                JOptionPane.showMessageDialog(null, "Tidak Ada Data");
            }else {
                JOptionPane.showMessageDialog(null, "Pilih Data Yang Akan Dihapus");
            }
        } else {
            Object primaryKey = daftarTable.getValueAt(daftarTable.getSelectedRow(), 0);
            model.removeRow(daftarTable.getSelectedRow());
            hapusDataDB(primaryKey);
        }
    }//GEN-LAST:event_btnHapusActionPerformed

    private void tfCariMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tfCariMouseClicked
        tfCari.setText("");
    }//GEN-LAST:event_tfCariMouseClicked

    private void tfCariKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tfCariKeyTyped
        if (evt.getKeyChar() == '\n'){ // agar saat menekan tombol enter langsung melakukan pencarian
            performSearch();
        }
    }//GEN-LAST:event_tfCariKeyTyped

    private void btnCariActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCariActionPerformed
        performSearch();
    }//GEN-LAST:event_btnCariActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        editSelectedRow();
    }//GEN-LAST:event_btnEditActionPerformed

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Penyewa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Penyewa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Penyewa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Penyewa.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Penyewa().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private table.TableCustom CTable;
    private Custom.FButton btnCari;
    private Custom.FButton btnEdit;
    private Custom.FButton btnHapus;
    private Custom.FButton btnTambah;
    private javax.swing.JTable daftarTable;
    private Custom.FButton fButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private Custom.JPanelGradient jPanelGradient1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField tfCari;
    // End of variables declaration//GEN-END:variables
}
