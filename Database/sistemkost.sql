-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 20, 2023 at 07:00 AM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sistemkost`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_indekos`
--

CREATE TABLE `data_indekos` (
  `nama` varchar(50) NOT NULL,
  `nik` bigint(16) NOT NULL,
  `hp` bigint(13) NOT NULL,
  `status` varchar(50) NOT NULL,
  `kesibukan` varchar(50) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `tipe` varchar(30) NOT NULL,
  `harga` bigint(50) NOT NULL,
  `tanggal` varchar(100) NOT NULL,
  `durasi` int(10) NOT NULL,
  `total` bigint(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `data_indekos`
--

INSERT INTO `data_indekos` (`nama`, `nik`, `hp`, `status`, `kesibukan`, `gender`, `tipe`, `harga`, `tanggal`, `durasi`, `total`) VALUES
('Rizky Gunawan', 6171050203040002, 89631213121, 'Belum Menikah', 'Mahasiswa', 'Laki-Laki', 'Tipe C', 1000000, '10-1-2024', 6, 6000000);

-- --------------------------------------------------------

--
-- Table structure for table `data_kamar`
--

CREATE TABLE `data_kamar` (
  `jenis` varchar(30) NOT NULL,
  `harga` bigint(20) NOT NULL,
  `jumlah` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `data_kamar`
--

INSERT INTO `data_kamar` (`jenis`, `harga`, `jumlah`) VALUES
('Tipe A', 500000, 10),
('Tipe B', 700000, 10),
('Tipe C', 1000000, 10),
('Tipe D', 1300000, 10),
('Tipe E', 1500000, 10);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jabatan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`username`, `password`, `nama`, `jabatan`) VALUES
('admin', 'admin', 'Master Admin', 'Owner'),
('rizky', 'rizky', 'rizky gunawan', 'Owner');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_indekos`
--
ALTER TABLE `data_indekos`
  ADD PRIMARY KEY (`nama`);

--
-- Indexes for table `data_kamar`
--
ALTER TABLE `data_kamar`
  ADD PRIMARY KEY (`jenis`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
